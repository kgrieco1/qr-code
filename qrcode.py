import pyqrcode
from PIL import Image

qrobj = pyqrcode.create('https://secure.actblue.com/donate/labor-for-bernie-2020-1?recurring=1')
with open('qrcode.png', 'wb') as f:
    qrobj.png(f, scale=10)

img = Image.open('qrcode.png')
width, height = img.size

logo_size = 200
logo = Image.open('LaborForBernie.png')

xmin = ymin = int((width / 2) - (logo_size / 2))
xmax = ymax = int((width / 2) + (logo_size / 2))

logo = logo.resize((xmax - xmin, ymax - ymin))

img.paste(logo, (xmin, ymin, xmax, ymax))

img.show()
img.save('qrcodefinal_blackandwhite.png')
