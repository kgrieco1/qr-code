import pyqrcode
from PIL import Image
url = pyqrcode.QRCode('https://secure.actblue.com/donate/labor-for-bernie-2020-1?recurring=1',error = 'H')
url.png('qrcode.png',scale=10)
im = Image.open('qrcode.png')
im = im.convert("RGBA")
logo = Image.open('LaborForBernie.png')
box = (135,135,235,235)
im.crop(box)
region = logo
region = region.resize((box[2] - box[0], box[3] - box[1]))
im.paste(region,box)
im.show()
